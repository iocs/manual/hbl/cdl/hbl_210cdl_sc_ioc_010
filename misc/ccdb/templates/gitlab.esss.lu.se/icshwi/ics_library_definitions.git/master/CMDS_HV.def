###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ## 
##                                                                                          ##  
##                          HV - Hand valve with position feedback							##
##                                                                                          ##  
##                                                                                          ##
############################         Version: 1.0             ################################
# Author:  Adalberto Fontoura
# Date:    11-11-2024
# Version: v1.0
# Changes: First Version adapted for CMDS Hand valves



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_Auto",          	PV_DESC="Operation Mode Auto",		PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Forced",           	PV_DESC="Operation Mode Forced", 	PV_ONAM="True",           PV_ZNAM="False")

#Valve physical states
add_digital("Opened",  	ARCHIVE=True,   PV_DESC="Valve Opened",          	PV_ONAM="True",           PV_ZNAM="False")
add_digital("Closed",  	ARCHIVE=True,   PV_DESC="Valve Closed",          	PV_ONAM="True",           PV_ZNAM="False")
add_analog("ValveColor",               "INT",                           PV_DESC="BlockIcon valve color")

add_digital("Inhibit_Manual",          	PV_DESC="Inhibit Manual Mode",   	PV_ONAM="InhibitManual",  PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           	PV_DESC="Inhibit Force Mode",    	PV_ONAM="InhibitForce",   PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",       PV_ONAM="InhibitLocking", PV_ZNAM="AllowLocking")

#for OPI visualization
add_digital("En_Auto_Btn",        		PV_DESC="Enable Auto  Button",		PV_ONAM="True",           PV_ZNAM="False")
add_digital("En_Force_Btn",         	PV_DESC="Enable Force Button",  	PV_ONAM="True",           PV_ZNAM="False")
add_digital("En_ForceVal_Btn",         	PV_DESC="Enable Manual Button",  	PV_ONAM="True",           PV_ZNAM="False")

#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device Locked",         PV_ONAM="True",           PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                          PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                          PV_DESC="Guest Lock ID")

#Alarm signals
add_major_alarm("LatchAlarm",          "Latching of alarms",            PV_ZNAM="True")
add_major_alarm("GroupAlarm",          "Group Alarm",                   PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "IO Error",                      PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",         PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",					PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Force",				PV_DESC="CMD: Force Mode")
add_digital("Cmd_Force_Open",			PV_DESC="CMD: Force Opened Status")
add_digital("Cmd_Force_Close",			PV_DESC="CMD: Force Closed Status")
add_digital("Cmd_ForceVal",				PV_DESC="CMD: Force Value")

add_digital("Cmd_AckAlarm",    			PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceUnlock",         PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",             PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",           PV_DESC="CMD: Unlock Device")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                          PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                          PV_DESC="Device ID after Blockicon Open")